#include <SFML/Graphics.hpp>


class Line : public sf::ConvexShape
{
public:
	Line(const sf::Vector2f& pt1 = sf::Vector2f(), const sf::Vector2f& pt2 = sf::Vector2f(), float thick = 1.0f)
	{
		// This could be solved with 4 vertices, but it'll need some boring math calculations...
		sf::Vector2f pt[8];
		pt[0] = pt1 - sf::Vector2f((thick / 2.0f), (thick / 2.0f));
		pt[1] = pt1 + sf::Vector2f((thick / 2.0f), (thick / 2.0f));
		pt[2] = pt2 + sf::Vector2f((thick / 2.0f), (thick / 2.0f));
		pt[3] = pt2 - sf::Vector2f((thick / 2.0f), (thick / 2.0f));
		pt[4] = pt1 + sf::Vector2f(-(thick / 2.0f), (thick / 2.0f));
		pt[5] = pt1 - sf::Vector2f(-(thick / 2.0f), (thick / 2.0f));
		pt[6] = pt2 - sf::Vector2f(-(thick / 2.0f), (thick / 2.0f));
		pt[7] = pt2 + sf::Vector2f(-(thick / 2.0f), (thick / 2.0f));

		setPointCount(8);
		for(int i=0; i<8; i++)
			setPoint(i, pt[i]);
	}
};