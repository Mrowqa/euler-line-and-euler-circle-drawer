Simple application with awful code written without planning in the shortest
possible time, which draws Euler line and Euler circle. It's possible to move
vertices of triangle and draw heights, normals and medians, too.

Official online repository:
https://bitbucket.org/Mrowqa/euler-line-and-euler-circle-drawer

~Mrowqa