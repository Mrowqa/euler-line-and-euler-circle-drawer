#pragma once
#include <SFML/Graphics.hpp>
#include "Line.h"

class Triangle :
	public sf::Drawable
{
public:
							Triangle(const sf::RenderWindow& window);
							~Triangle(void);

	void					onEvent(const sf::Event& event);
	void					update(); // we don't need dt

private:
	class Property :
		public sf::Drawable {
	public:
		sf::Vector2f	segments[3][2];
		sf::Vector2f	intersectionPoint;
		sf::Color		lineColor;
		sf::Color		pointColor;
		bool			shouldBeDrawed;

						Property() :
							shouldBeDrawed(true) {}

	private:
		virtual void	draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};

private:
	virtual void			draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Vector2f			vertices[3];
	Property				heights, normals, medians;
	sf::Vector2f			expandedEdges[3][2];
	sf::Vector2f			eulerLine[2];
	bool					shouldBeEulerLineBeDrawed;
	sf::Vector2f			eulerCirclePoints[9];
	sf::Vector2f			eulerCircleCenter;
	bool					shouldBeEulerCircleBeDrawed;
	const sf::RenderWindow& window;

	int						selectedVertex;
	bool					isVertexDragged;
	sf::Vector2f			lastMousePos;
	sf::Clock				stopwatch;
};

