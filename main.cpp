#include <SFML/Graphics.hpp>
#include "Triangle.h"
#include <iostream>

/*
TODO (propositions):
- drawing value of angles and length of triangle's segments
- modifying these values
- better drawing (expanding) euler line
- moving vertex on straight line when shift/any key pressed
- drawing other stuff?
- any new ideas?
*/
int main()
{
	std::cout << "Mysz:\n" // Mouse
		"[LPM]    Przesun wierzcholek trojkata\n" // [LMB] Move vertex of triangle
		"[Scroll] Przybliz/oddal obraz\n" // [Scroll] Zoom in/out
		"[PPM]    Przesun obraz\n\n" // [RMB] Move camera
		"Klawisze:\n" // Keys
		"[Q] Wlacz/Wylacz rysowanie wysokosci\n" // [Q] Enable/disable drawing heights
		"[W] Wlacz/Wylacz rysowanie symetralnych\n" // [W] Enable/disable drawing normals
		"[E] Wlacz/Wylacz rysowanie srodkowych\n" // [E] Enable/disable drawing medians
		"[R] Wlacz/Wylacz rysowanie prostej eulera\n" // [R] Enable/disable drawing euler line
		"[T] Wlacz/Wylacz rysowanie okregu eulera\n\n\n" // [T] Enable/disable drawing euler circle
		"v0.84.pl beta\n"
		"(C) 2014 Artur \"Mrowqa\" Jamro, http://mrowqa.pl\n";

	sf::ContextSettings contexSettings;
	contexSettings.antialiasingLevel = 8;
	sf::RenderWindow window(sf::VideoMode(800, 600), "Prosta Eulera i okrag 9-ciu punktow", sf::Style::Default, contexSettings);
	window.setFramerateLimit(60);
	Triangle triangle(window);

	sf::View view = window.getView();
	sf::Vector2i lastMousePixelPos = sf::Mouse::getPosition(window);

    while(window.isOpen())
    {
        sf::Event event;
        while(window.pollEvent(event))
        {
			triangle.onEvent(event);
			switch(event.type) {
			case sf::Event::Closed:
                 window.close();
				 break;
			
			case sf::Event::MouseWheelMoved: {
					float factor = -event.mouseWheel.delta;
					if(factor > 0)
						factor += 1.f;
					if(factor < 0)
						factor = 1/(-factor+1);
					view.zoom(sqrt(factor));
					sf::Vector2f dPos = sf::Vector2f(window.getSize()) / 2.f - sf::Vector2f(sf::Mouse::getPosition(window));
					view.move(-dPos / 3.f * (window.getView().getSize().x/800.f));
				}
				break;

			case sf::Event::MouseMoved: {
					sf::Vector2i dPos(event.mouseMove.x - lastMousePixelPos.x, event.mouseMove.y - lastMousePixelPos.y);
					if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
						view.move(sf::Vector2f(-dPos) / 3.f * (window.getView().getSize().x/800.f));
					lastMousePixelPos += dPos;
				}
				break;

			default:
				break;
			}
            
        }
		lastMousePixelPos = sf::Mouse::getPosition(window);
		triangle.update();

		window.setView(view);
		window.clear(sf::Color::White);
		window.draw(triangle);
        window.display();
    }

    return 0;
}