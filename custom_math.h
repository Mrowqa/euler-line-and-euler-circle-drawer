#include <SFML/System.hpp>
#include <algorithm>

typedef long double LD;

inline LD dot(const sf::Vector2f& a, const sf::Vector2f& b) {
	return a.x*b.x + a.y*b.y;
}

inline sf::Vector2f castPointOnLine(const sf::Vector2f& lineBeg, const sf::Vector2f& lineEnd,
	const sf::Vector2f& point) {
	LD f = dot(lineEnd-lineBeg, point-lineBeg) / dot(lineEnd-lineBeg, lineEnd-lineBeg);
	return sf::Vector2f(lineBeg.x + f*(lineEnd.x-lineBeg.x),
		lineBeg.y + f*(lineEnd.y-lineBeg.y));
}

inline sf::Vector3f getLineEquation(const sf::Vector2f& segBeg, const sf::Vector2f& segEnd) {
	return sf::Vector3f(segBeg.y - segEnd.y, // A
		segEnd.x - segBeg.x, // B
		segBeg.x*(segEnd.y-segBeg.y) - segBeg.y*(segEnd.x-segBeg.x)); // C
}

inline sf::Vector2f getIntersectionPoint(const sf::Vector3f& a, const sf::Vector3f& bx) {
	sf::Vector3f b(bx);
	if(b.x==0) b.x=1; // sic!
	float y = (b.z*a.x/b.x-a.z)/(a.y-b.y*a.x/b.x);
	float x = (-a.y*y-a.z)/a.x;
	return sf::Vector2f(x, y);
}

inline sf::Vector2f getMidpoint(const sf::Vector2f& pt1, const sf::Vector2f& pt2) {
	return sf::Vector2f((pt1.x+pt2.x)/2.f, (pt1.y+pt2.y)/2.f);
}

inline sf::Vector3f getNormal(const sf::Vector3f& line) {
	if(line.y != 0) 
		return sf::Vector3f(1, -line.x/line.y, 0);
	return sf::Vector3f(0, 1, 0);
}

inline sf::Vector3f offsetLineThroughPoint(const sf::Vector3f& line, const sf::Vector2f& point) {
	sf::Vector3f res = line;
	res.z = -line.x*point.x-line.y*point.y;
	return res;
}

inline sf::Vector2f computeSomePoint(const sf::Vector3f& line) {
	if(line.y != 0)
		return sf::Vector2f(0, -line.z/line.y);
	else
		return sf::Vector2f(-line.z/line.x, 0);
}

inline sf::FloatRect getAABB(const sf::Vector2f& pt1, const sf::Vector2f& pt2) {
	float minx = std::min(pt1.x, pt2.x);
	float maxx = std::max(pt1.x, pt2.x);
	float miny = std::min(pt1.y, pt2.y);
	float maxy = std::max(pt1.y, pt2.y);
	return sf::FloatRect(minx, miny, maxx-minx, maxy-miny);
}

inline sf::FloatRect extendAABB(const sf::FloatRect& aabb, const sf::Vector2f& pt) {
	float minx = std::min(aabb.left, pt.x);
	float maxx = std::max(aabb.left+aabb.width, pt.x);
	float miny = std::min(aabb.top, pt.y);
	float maxy = std::max(aabb.top+aabb.height, pt.y);
	return sf::FloatRect(minx, miny, maxx-minx, maxy-miny);
}

inline LD vectorValue(const sf::Vector2f& vec) {
	return sqrt(vec.x*vec.x + vec.y*vec.y);
}

inline LD pointsDistance(const sf::Vector2f& pt1, const sf::Vector2f& pt2) {
	return vectorValue(pt2-pt1);
}

inline sf::Vector2f getUnitVector(const sf::Vector2f& vec) {
	return vec / static_cast<float>(vectorValue(vec));
}

inline void expandSegment(sf::Vector2f& beg, sf::Vector2f& end, const sf::Vector2f& dest, float overlen=30.f) {
	sf::FloatRect aabb = getAABB(beg, end);
	aabb.left += overlen; aabb.top += overlen;
	aabb.width -= 2*overlen; aabb.height -= 2*overlen;
	if(aabb.contains(dest))
		return;

	if(pointsDistance(end,dest) > pointsDistance(beg,dest))
		std::swap(end, beg);
	sf::Vector2f endBackup = end;

	end = dest;
	end += getUnitVector(end-beg)*overlen;

	if(pointsDistance(beg,end) < pointsDistance(beg,endBackup))
		end = endBackup;
}