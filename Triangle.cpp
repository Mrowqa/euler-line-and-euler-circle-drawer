#include "Triangle.h"
#include "custom_math.h"
#include <cmath>

// ------------------------------------------------------------------
Triangle::Triangle(const sf::RenderWindow& window)
	:
	vertices(),
	selectedVertex(-1),
	isVertexDragged(false),
	window(window),
	shouldBeEulerLineBeDrawed(true),
	shouldBeEulerCircleBeDrawed(true)
{
	//for(int i=0; i<3; i++)
	//	vertices[i] = sf::Vector2f(i==1?200:100, i==2?100:200);
	vertices[0] = sf::Vector2f(400, 200);
	vertices[1] = sf::Vector2f(400+200/sqrt(3), 400);
	vertices[2] = sf::Vector2f(400-200/sqrt(3), 400);

	heights.lineColor = sf::Color::Blue;
	heights.pointColor = sf::Color::Cyan;
	normals.lineColor = sf::Color::Green;
	normals.pointColor = sf::Color(0, 128, 0);
	medians.lineColor = sf::Color::Red;
	medians.pointColor = sf::Color::Magenta;
}

// ------------------------------------------------------------------
Triangle::~Triangle(void)
{
}


// ------------------------------------------------------------------
void Triangle::onEvent(const sf::Event& event)
{
	switch(event.type) {

	case sf::Event::MouseButtonPressed:
		if(selectedVertex != -1 && event.mouseButton.button == sf::Mouse::Left) {
			isVertexDragged = true;
			lastMousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
		}
		break;

	case sf::Event::MouseButtonReleased:
		if(event.mouseButton.button == sf::Mouse::Left)
			isVertexDragged = false;
		break;

	case sf::Event::KeyPressed:
		switch(event.key.code) {
		case sf::Keyboard::Q:
			heights.shouldBeDrawed = !heights.shouldBeDrawed;
			break;
		case sf::Keyboard::W:
			normals.shouldBeDrawed = !normals.shouldBeDrawed;
			break;
		case sf::Keyboard::E:
			medians.shouldBeDrawed = !medians.shouldBeDrawed;
			break;
		case sf::Keyboard::R:
			shouldBeEulerLineBeDrawed = !shouldBeEulerLineBeDrawed;
			break;
		case sf::Keyboard::T:
			shouldBeEulerCircleBeDrawed = !shouldBeEulerCircleBeDrawed;
			break;
		default:
			break;
		}
		break;

	default:
		break;
	}
}

// ------------------------------------------------------------------
void Triangle::update()
{
	sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));

	if(isVertexDragged) {
		sf::Vector2f dPos = mousePos - lastMousePos;
		lastMousePos = mousePos;
		vertices[selectedVertex] += dPos;
	}
	else {
		selectedVertex = -1;

		for(int i=0; i<3; i++)
		{
			sf::Vector2f dPos = mousePos - vertices[i];
			float distance = sqrtf(dPos.x*dPos.x+dPos.y*dPos.y);

			if(distance < 10.f)
				selectedVertex = i;
		}
	}

	for(int i=0; i<3; i++) {
		expandedEdges[i][0] = vertices[(i+1)%3];
		expandedEdges[i][1] = vertices[(i+2)%3];
	}

	// heights
	for(int i=0; i<3; i++) {
		heights.segments[i][0] = vertices[i];
		heights.segments[i][1] = castPointOnLine(vertices[(i+1)%3], vertices[(i+2)%3], vertices[i]);
	}
	heights.intersectionPoint = getIntersectionPoint(getLineEquation(heights.segments[0][0], heights.segments[0][1]),
		getLineEquation(heights.segments[1][0], heights.segments[1][1]));
	for(int i=0; i<3; i++) {
		expandSegment(heights.segments[i][0], heights.segments[i][1], heights.intersectionPoint);
		sf::Vector2f intersectionPoint = getIntersectionPoint(
			getLineEquation(heights.segments[i][0], heights.segments[i][1]),
			getLineEquation(expandedEdges[i][0], expandedEdges[i][1]));
		expandSegment(expandedEdges[i][0], expandedEdges[i][1], intersectionPoint);
	}

	// normals
	for(int i=0; i<3; i++) {
		sf::Vector3f normal = offsetLineThroughPoint(
			getNormal(getLineEquation(vertices[(i+1)%3], vertices[(i+2)%3])),
			getMidpoint(vertices[(i+1)%3], vertices[(i+2)%3]));

		normals.segments[i][0] = getMidpoint(vertices[(i+1)%3], vertices[(i+2)%3]);
		normals.segments[i][1] = castPointOnLine(normals.segments[i][0], computeSomePoint(normal), vertices[i]);
		expandSegment(normals.segments[i][0], normals.segments[i][1], sf::Vector2f(normals.segments[i][0]));
	}
	normals.intersectionPoint = getIntersectionPoint(getLineEquation(normals.segments[0][0], normals.segments[0][1]),
		getLineEquation(normals.segments[1][0], normals.segments[1][1]));
	for(int i=0; i<3; i++)
		expandSegment(normals.segments[i][0], normals.segments[i][1], normals.intersectionPoint);

	// medians
	for(int i=0; i<3; i++) {
		medians.segments[i][0] = vertices[i];
		medians.segments[i][1] = getMidpoint(vertices[(i+1)%3], vertices[(i+2)%3]);
	}
	medians.intersectionPoint = getIntersectionPoint(getLineEquation(medians.segments[0][0], medians.segments[0][1]),
		getLineEquation(medians.segments[1][0], medians.segments[1][1]));

	// euler line
	sf::Vector2f offset1 = heights.intersectionPoint-medians.intersectionPoint;
	sf::Vector2f offset2 = normals.intersectionPoint-medians.intersectionPoint;
	sf::Vector2f offset = 2.0f * (vectorValue(offset1) > vectorValue(offset2) ? offset1 : offset2);
	eulerLine[0] = medians.intersectionPoint - offset;
	eulerLine[1] = medians.intersectionPoint + offset;

	// euler circle
	eulerCircleCenter = getMidpoint(heights.intersectionPoint, normals.intersectionPoint);
	for(int i=0; i<3; i++)
		eulerCirclePoints[i] = castPointOnLine(vertices[(i+1)%3], vertices[(i+2)%3], vertices[i]);
	for(int i=3; i<6; i++)
		eulerCirclePoints[i] = getMidpoint(vertices[(i+1)%3], vertices[(i+2)%3]);
	for(int i=6; i<9; i++)
		eulerCirclePoints[i] = getMidpoint(vertices[i%3], heights.intersectionPoint);
}

// ------------------------------------------------------------------
#define ABS(x) ((x)<0?-(x):(x))
void Triangle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	Line line;
	if(heights.shouldBeDrawed)
		for(int i=0; i<3; i++) {
			line = Line(expandedEdges[i][0], expandedEdges[i][1], 1.f);
			line.setFillColor(sf::Color(127,127,127));
			target.draw(line, states);
		}
	for(int i=0; i<3; i++) {
		line = Line(vertices[i], vertices[(i+1)%3], 3);
		line.setFillColor(sf::Color::Black);
		target.draw(line, states);
	}

	if(selectedVertex != -1) {
		sf::CircleShape circle(ABS(sin(stopwatch.getElapsedTime().asSeconds()))*5+5);
		circle.setOrigin(circle.getLocalBounds().width/2.f, circle.getLocalBounds().height/2.f);
		circle.setPosition(vertices[selectedVertex]);
		circle.setFillColor(sf::Color::Transparent);
		circle.setOutlineColor(sf::Color(127, 127, 127));
		circle.setOutlineThickness(2);

		target.draw(circle, states);
	}

	if(heights.shouldBeDrawed)
		target.draw(heights, states);
	if(normals.shouldBeDrawed)
		target.draw(normals, states);
	if(medians.shouldBeDrawed)
		target.draw(medians, states);

	if(shouldBeEulerLineBeDrawed) {
		Line line(eulerLine[0], eulerLine[1], 3);
		line.setFillColor(sf::Color(100, 100, 100));
		target.draw(line, states);
	}
	if(shouldBeEulerCircleBeDrawed) {
		float radius = pointsDistance(eulerCircleCenter, eulerCirclePoints[0]);
		float thick = 3.f;

		// draw euler circle
		sf::CircleShape eulerCircle(radius - thick/2.f, 100);
		eulerCircle.setOrigin(eulerCircle.getLocalBounds().width/2.f, eulerCircle.getLocalBounds().height/2.f);
		eulerCircle.setPosition(eulerCircleCenter);
		eulerCircle.setFillColor(sf::Color::Transparent);
		eulerCircle.setOutlineThickness(thick);
		eulerCircle.setOutlineColor(sf::Color(130, 130, 130));
		target.draw(eulerCircle);

		// draw center point
		sf::CircleShape circle(5);
		circle.setFillColor(sf::Color(160, 120, 30));
		circle.setOrigin(circle.getLocalBounds().width/2.f, circle.getLocalBounds().height/2.f);
		circle.setPosition(eulerCircleCenter);
		target.draw(circle);

		// draw nine points
		for(int i=0; i<9; i++) {
			sf::CircleShape circle(5);
			circle.setFillColor(i<3?heights.pointColor:i<6?medians.pointColor:sf::Color(0,0,180));
			circle.setOrigin(circle.getLocalBounds().width/2.f, circle.getLocalBounds().height/2.f);
			circle.setPosition(eulerCirclePoints[i]);
			target.draw(circle);
		}
	}
}

// ------------------------------------------------------------------
void Triangle::Property::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for(int i=0; i<3; i++) {
		Line line(segments[i][0], segments[i][1], 2);
		line.setFillColor(lineColor);
		target.draw(line, states);
	}

	sf::CircleShape circle(5);
	circle.setFillColor(pointColor);
	circle.setOrigin(circle.getLocalBounds().width/2.f, circle.getLocalBounds().height/2.f);
	circle.setPosition(intersectionPoint);
	target.draw(circle);
}
